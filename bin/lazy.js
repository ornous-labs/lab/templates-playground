#!/usr/bin/env node

const program = require('commander')
const fs = require('fs')
const path = require('path')

const { renderTemplate } = require('../utils')

program
  .version('0.1.0')

program
  .command('create <templateName>')
  .option('-o, --outputPath [path]', 'the output directory', 'dist/')
  .option('-p, --port [port]', 'port', '8000')
  .action((templateName, { outputPath, ...args } ) => {
    if (!fs.existsSync(path.dirname(outputPath))) {
      console.error(`
        specified location does not exist.
        Please create the directory first ${outputPath}
      `)
      process.exit(1)
    }

    const dataBag = Object.assign({}, ...['port'].map(key => ({ [key]: args[key] })))

    renderTemplate(templateName, dataBag, outputPath)
  })

program.parse(process.argv)
