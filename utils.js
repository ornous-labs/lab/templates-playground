const nunjucks = require('nunjucks')
const fs = require('fs')

nunjucks.configure('templates', { autoescape: true })

module.exports = {
  renderTemplate: (fileName, dataBag = {}, outputPath) => {
    if (outputPath === null) {
      outputPath = `dist/${fileName}`
    }

    console.log(fileName, dataBag, outputPath)
    fs.writeFile(outputPath, nunjucks.render(fileName, dataBag), (err) => {
      if (err) {
        console.log(err)
        process.exit(1)
      }

      console.log(`The ${fileName} was saved!`)
    })
  }
}
